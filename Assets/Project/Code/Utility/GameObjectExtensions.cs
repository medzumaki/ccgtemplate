﻿using System.Collections.Generic;
using medzumi.Utilities.CodeExtensions;
using UnityEngine;

namespace Project.Code.Utility
{
    public static class GameObjectExtensions
    {
        public static List<T> CollectAllChild<T>(this GameObject fromGameObject, List<T> list = null) where T : Component
        {
            if (list.IsNull())
                list = new List<T>();
            
            foreach (Transform transform in fromGameObject.transform)
            {
                if (transform.TryGetComponent(out T component))
                {
                    list.Add(component);
                }
                else
                {
                    transform.gameObject.CollectAllChild<T>(list);
                }
            }

            return list;
        }
        
        public static int CollectAllChildForEditor<T>(this Component fromComponent, List<T> list) where T : Component
        {
            if (list.IsNull())
                list = new List<T>();

            var count = 0;
            foreach (Transform transform in fromComponent.transform)
            {
                if (transform.TryGetComponent(out T component))
                {
                    list.Add(component);
                    count++;
                }
                else
                {
                    transform.gameObject.CollectAllChild<T>(list);
                }
            }

            #if UNITY_EDITOR
            if (count > 0)
            {
                UnityEditor.EditorUtility.SetDirty(fromComponent);
            }
            #endif
            return count;
        }
    }
}