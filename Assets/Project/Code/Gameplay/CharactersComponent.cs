﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using Pixeye.Actors;
using PlayFab;
using PlayFab.ServerModels;
using Project.Code.FishNet;
using Project.Code.Gameplay.Character;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Project.Code.Gameplay
{
    [RequireComponent(typeof(PlayFabComponent))]
    public class CharactersComponent : NetworkActor<CharactersComponent>
    {
        private PlayFabComponent _playFabComponent;

        [field: SyncObject(ReadPermissions = ReadPermission.OwnerOnly, SendRate = 1)]
        public readonly SyncList<CharacterComponent> CharactersList = new SyncList<CharacterComponent>();

        [SerializeField]
        private List<CharacterComponent> _characterComponentPrefabs = new List<CharacterComponent>();
        
        private List<ICharactersHandler> _charactersHandlers = new List<ICharactersHandler>();

        protected void Awake()
        {
            _playFabComponent = GetComponent<PlayFabComponent>();
            GetComponents(_charactersHandlers);
        }

        public override void OnSpawnServer(NetworkConnection connection)
        {
            base.OnSpawnServer(connection);
            if (_playFabComponent)
            {
                CharactersList.Clear();
                PlayFabServerAPI.GetAllUsersCharacters(new ListUsersCharactersRequest()
                {
                    PlayFabId = _playFabComponent.PlayFabId.Value
                }, x =>
                {
                    foreach (var characterResult in x.Characters)
                    {
                        Spawn(characterResult.CharacterType, characterResult.CharacterId,
                            characterResult.CharacterName, connection);
                    }
                    _charactersHandlers.ForEach(x => x.Handle(this));
                }, y =>
                {
                });
            }
        }

        private bool Spawn(string characterType, string characterId, string characterName, NetworkConnection connection)
        {
            if (_characterComponentPrefabs.FirstOrDefault(x =>
                string.Equals(x.name, characterType)) is { } character)
            {
                var nob = NetworkManager.GetPooledInstantiated(character.gameObject, true);
                if (nob.TryGetComponent(out CharacterComponent characterComponent))
                {
                    characterComponent.Name.Value = characterName;
                    characterComponent.Id.Value = characterId;
                    nob.transform.SetParent(transform);
                    NetworkManager.ServerManager.Spawn(nob, connection);
                    CharactersList.Add(characterComponent);
                    return true;
                }
                else
                {
                    nob.Despawn();
                    return false;
                }
            }

            return false;
        }

        [ServerRpc]
        public void CreateCharacter(string name, string characterType, Dictionary<string, string> tags = null)
        {
#if ENABLE_PLAYFABSERVER_API
            var grantCharacterToUserRequest = new GrantCharacterToUserRequest()
            {
                CharacterName = name,
                CharacterType = characterType,
                CustomTags = tags
            };
            
            PlayFabServerAPI.GrantCharacterToUser(grantCharacterToUserRequest,
    x =>
            {
                Spawn(characterType, x.CharacterId, characterType, base.Owner);
                _charactersHandlers.ForEach(x => x.Handle(this));
            }, y =>
            {
                
            });
#endif
        }

        protected override CharactersComponent ProvideComponent()
        {
            return this;
        }
    }
    
    #region HELPERS

    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    static class CharactersHelper
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static ref CharactersComponent ComponentObserver(in this ent entity) =>
            ref Storage<CharactersComponent>.components[entity.id];
    }
    
    public class CharactersStorage : Storage<CharactersComponent>
    {
        public override void Dispose(indexes disposed)
        {
            
        }
    }
    #endregion
}