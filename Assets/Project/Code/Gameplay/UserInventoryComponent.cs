﻿using System;
using System.Runtime.CompilerServices;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using Pixeye.Actors;
using PlayFab;
using PlayFab.ServerModels;
using Project.Code.FishNet;
using UniRx;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using UnityEngine.Scripting;
using Zenject;

namespace Project.Code.Gameplay
{
    [RequireComponent(typeof(PlayFabComponent))]
    public class UserInventoryComponent : NetworkActor<UserInventoryComponent>
    {
        private PlayFabComponent _characterComponent;

        [field: SyncObject]
        public readonly SyncDictionary<string, int> Currencies = new SyncDictionary<string, int>();

        private void Awake()
        {
            if (!TryGetComponent(out _characterComponent))
            {
                Destroy(this);
            }
        }

        public override void OnSpawnServer(NetworkConnection connection)
        {
            base.OnSpawnServer(connection);
            _characterComponent.PlayFabId
                .Subscribe(x =>
                {
                    if (connection != null)
                    {
                        PlayFabServerAPI.GetUserInventory(new GetUserInventoryRequest()
                        {
                            PlayFabId = x
                        }, xx =>
                        {
                            foreach (var keyValuePair in xx.VirtualCurrency)
                            {
                                Currencies[keyValuePair.Key] = keyValuePair.Value;
                            }
                            foreach (NetworkObject child in NetworkObject.ChildNetworkObjects)
                            {
                                child.Despawn();
                            }
                            foreach (var itemInstance in xx.Inventory)
                            {
                                var load = Resources.Load<NetworkObject>(itemInstance.ItemId);
                                var nob = NetworkManager.GetPooledInstantiated(load, true);
                                nob.transform.SetParent(transform);
                                NetworkManager.ServerManager.Spawn(nob, connection);
                            }
                        }, yy =>
                        {

                        });
                    }
                })
                .AddTo(this);
        }

        protected override UserInventoryComponent ProvideComponent()
        {
            return this;
        }
    }
    
    #region HELPERS

    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Preserve]
    static class UserInventoryHelper
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [Preserve]
        internal static ref UserInventoryComponent ComponentObserver(in this ent entity) =>
            ref Storage<UserInventoryComponent>.components[entity.id];
    }
    
    public class UserInventoryStorage : Storage<UserInventoryComponent>
    {
        public override void Dispose(indexes disposed)
        {
            
        }
    }
    #endregion
}