﻿using System.Linq;
using UnityEngine;

namespace Project.Code.PresenterLayer
{
    [CreateAssetMenu]
    public class DevelopScriptableAssetProvider : ScriptableAssetProvider
    {
        [SerializeField] private medzumi.Utilities.GenericPatterns.Datas.ValueTuple<string, Object>[] _valueTuples =
            new medzumi.Utilities.GenericPatterns.Datas.ValueTuple<string, Object>[0];
        
        public override T GetAsset<T>(string key)
        {
            return _valueTuples.FirstOrDefault(x => x.Item1 == key && x.Item2 is T).Item2 as T;
        }
    }
}