﻿using UnityEngine;

namespace Project.Code.PresenterLayer
{
    public class NonPresenter : Presenter
    {
        protected override void Awake()
        {
            
        }

        protected override void Rebuild()
        {
        }

        protected override void PresentHandler(GameObject presentableGameObject)
        {
        }

        protected override bool CheckAvailablePresent(GameObject presentableGameObject)
        {
            return false;
        }

        protected override void StopPresentHandler()
        {
        }
    }
}