﻿using System;
using System.Collections.Generic;
using FishNet;
using FishNet.Authenticating;
using FishNet.Connection;
using FishNet.Managing;
using FishNet.Managing.Scened;
using FishNet.Transporting;
using FishNet.Utility;
using PlayFab;
using PlayFab.ServerModels;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Project.Code.Authentication.PlayFab.Server
{
    public class PlayFabAuthenticator : Authenticator
    {
        public override event Action<NetworkConnection, bool> OnAuthenticationResult;

        private readonly Dictionary<string, NetworkConnection> _keyToConnection =
            new Dictionary<string, NetworkConnection>();

        private readonly Dictionary<NetworkConnection, string> _connectionToKey =
            new Dictionary<NetworkConnection, string>();

        [Scene][SerializeField] private string _afterLogInSceneName;

        public IReadOnlyDictionary<NetworkConnection, string> NetworkConnections => _connectionToKey;

        public override void InitializeOnce(NetworkManager networkManager)
        {
            base.InitializeOnce(networkManager);
            networkManager.ServerManager.RegisterBroadcast<SessionTicketAuthentication>(TokenAuthenticate, false);
            networkManager.ServerManager.OnRemoteConnectionState += RemoteConnectionState;
        }

        private void RemoteConnectionState(NetworkConnection arg1, RemoteConnectionStateArgs arg2)
        {
            if (arg2.ConnectionState == global::FishNet.Transporting.RemoteConnectionState.Stopped)
            {
                if (_connectionToKey.TryGetValue(arg1, out var key))
                {
                    _connectionToKey.Remove(arg1);
                    _keyToConnection.Remove(key);
                }
            }
        }

        private void TokenAuthenticate(NetworkConnection arg1, SessionTicketAuthentication arg2)
        {
#if ENABLE_PLAYFABSERVER_API
            PlayFabServerAPI.AuthenticateSessionTicket(new AuthenticateSessionTicketRequest()
            {
                SessionTicket = arg2.SessionTicket
            }, x =>
            {
                if (_keyToConnection.TryGetValue(x.UserInfo.PlayFabId, out var connection))
                {
                    foreach (var networkObject in connection.Objects)
                    {
                        networkObject.GiveOwnership(arg1);
                    }

                    connection.Disconnect(true);
                }

                _keyToConnection[x.UserInfo.PlayFabId] = arg1;
                _connectionToKey[arg1] = x.UserInfo.PlayFabId;
                OnAuthenticationResult?.Invoke(arg1, true);
                InstanceFinder.SceneManager.LoadConnectionScenes(arg1, new SceneLoadData()
                {
                    Options = new LoadOptions()
                    {
                        AllowStacking = true,
                        AutomaticallyUnload = true,
                        LocalPhysics = LocalPhysicsMode.None
                    },
                    SceneLookupDatas = new SceneLookupData[]
                    {
                        new SceneLookupData(_afterLogInSceneName)
                    },
                    ReplaceScenes = ReplaceOption.All
                });
            }, y =>
            {
                arg1.Broadcast(new Error.Error()
                {
                    
                });
                OnAuthenticationResult?.Invoke(arg1, false);
            });
#endif
        }
    }
}