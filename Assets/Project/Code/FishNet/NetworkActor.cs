﻿using System;
using System.Linq;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using Pixeye.Actors;
using Sirenix.OdinInspector;
using UnityEngine;
using Utilities.Unity.PropertyAttributes;

namespace Project.Code.FishNet
{
    [RequireComponent(typeof(Actor))]
    public class NetworkActor : NetworkBehaviour
    {
        [field: AutoBind(AutoBindType.Self)]
        [field: ReadOnly]
        [field: SerializeField]
        protected Actor _actor { get; private set; }
        
        public abstract class Condition : ScriptableObject
        {
            public abstract bool IsEnabled(NetworkActor networkActor);
        }

        [SerializeField] private Condition[] _conditions = new Condition[0];

        protected virtual bool IsEnabled() => _conditions.All(x => x.IsEnabled(this));

        protected virtual void SetActive(bool isActive = true)
        {
            
        }

        protected void SetActiveWithCheckEnable(bool isActive = true)
        {
            if (isActive)
            {
                SetActive(IsEnabled());
            }
            else if(IsEnabled())
            {
                SetActive(false);
            }
        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            LayerKernel.Obj.Init(gameObject);
            SetActiveWithCheckEnable();
        }

        public override void OnStopClient()
        {
            base.OnStopClient();
            SetActiveWithCheckEnable(false);
        }

        public override void OnSpawnServer(NetworkConnection connection)
        {
            base.OnSpawnServer(connection);
            LayerKernel.Obj.Init(gameObject);
            SetActiveWithCheckEnable();
        }

        public override void OnDespawnServer(NetworkConnection connection)
        {
            base.OnDespawnServer(connection);
            SetActiveWithCheckEnable(false);
        }
    }
    
    [RequireComponent(typeof(Actor))]
    public abstract class NetworkActor<T> : NetworkActor
    {
        protected abstract T ProvideComponent();

        protected override void SetActive(bool isActive = true)
        {
            base.SetActive(isActive);
            if (isActive)
            {
                _actor.entity.Set(ProvideComponent());
            }
            else
            {
                _actor.entity.Remove<T>();       
            }
        }
    }
}