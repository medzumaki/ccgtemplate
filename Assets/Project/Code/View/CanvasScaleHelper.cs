﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Project.Code.View
{
    [RequireComponent(typeof(CanvasScaler))]
    [ExecuteAlways]
    public class CanvasScaleHelper : MonoBehaviour
    {
        private CanvasScaler _canvasScaler;
        
        private void Start()
        {
            _canvasScaler = GetComponent<CanvasScaler>();
            UpdateCanvasScaler(_canvasScaler);
        }

        private void UpdateCanvasScaler(CanvasScaler canvasScaler)
        {
            var referenceScale = canvasScaler.referenceResolution.x / canvasScaler.referenceResolution.y;
            var screenScale = Screen.width / Screen.height;
            if (screenScale > referenceScale)
            {
                canvasScaler.matchWidthOrHeight = 1;
            }
            else
            {
                canvasScaler.matchWidthOrHeight = 0;
            }
        }
        
        #if UNITY_EDITOR
        private void Update()
        {
            UpdateCanvasScaler(_canvasScaler);
        }
        #endif
    }
}