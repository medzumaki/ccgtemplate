﻿using System;
using FishNet.Object.Synchronizing;
using Project.Code.Gameplay;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Project.Code.PresenterLayer
{
    public abstract class ScriptableAssetProvider : ScriptableObject
    {
        public abstract T GetAsset<T>(string key) where T : class;
    }

    public class UserCurrencyPresenter : Presenter<UserInventoryComponent>
    {
        [SerializeField] private string _key;
        [SerializeField] private TMP_Text _text;
        [SerializeField] private Image _image;
        [SerializeField] private ScriptableAssetProvider _scriptableAssetProvider;
        [SerializeField] private int _value;

        protected override void PresentHandler(GameObject presentableGameObject)
        {
            base.PresentHandler(presentableGameObject);
            _model.Currencies.OnChange += CurrenciesOnOnChange;
        }

        protected override void StopPresentHandler()
        {
            base.StopPresentHandler();
            _model.Currencies.OnChange -= CurrenciesOnOnChange;
        }

        private void CurrenciesOnOnChange(SyncDictionaryOperation op, string key, int value, bool asserver)
        {
            if (key == _key)
            {
                DrawCurrencyValue(value);
            }
        }

        private void DrawCurrency(string key)
        {
            if (_image)
            {
                _image.overrideSprite = _scriptableAssetProvider?.GetAsset<Sprite>(key);
            }
        }

        private void DrawCurrencyValue(int value)
        {
            if (_text)
            {
                _text.text = (_value = value).ToString();
            }
        }

        private void OnValidate()
        {
            DrawCurrency(_key);
            DrawCurrencyValue(_value);
        }
    }
}