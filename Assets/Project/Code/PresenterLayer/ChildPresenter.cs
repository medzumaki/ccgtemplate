﻿using System.Collections.Generic;
using medzumi.Utilities.Pooling;
using Sirenix.Utilities;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Project.Code.PresenterLayer
{
    [RequireComponent(typeof(IAssetProviderFor<GameObject, GameObject>))]
    public class ChildPresenter : Presenter
    {
        private Dictionary<int, (IPool<GameObject>, GameObject)> _gameObjects = new Dictionary<int, (IPool<GameObject>, GameObject)>();
        private Dictionary<int, (IPool<GameObject>, GameObject)> _copy = new Dictionary<int, (IPool<GameObject>, GameObject)>();
        
        private IAssetProviderFor<GameObject, GameObject> _assetProvider;

        protected override void Awake()
        {
            base.Awake();
            _assetProvider = GetComponent<IAssetProviderFor<GameObject, GameObject>>();
        }

        protected override void PresentHandler(GameObject presentableGameObject)
        {
            base.PresentHandler(presentableGameObject);
            presentableGameObject.transform
                .OnTransformChildrenChangedAsObservable()
                .Subscribe(OnTransformChildrenChanged)
                .AddTo(this);
            OnTransformChildrenChanged(default);
        }

        protected override void StopPresentHandler()
        {
            base.StopPresentHandler();
            _gameObjects.ForEach(x =>
            {
                x.Value.Item1.Release(x.Value.Item2);
            });
            _gameObjects.Clear();
        }

        private void OnTransformChildrenChanged(Unit unit)
        {
            _copy.Clear();
            foreach (Transform child in _presentableGameObject.transform)
            {
                var instanceId = child.gameObject.GetInstanceID();
                var prefab = _assetProvider.Provide(child.gameObject);
                var pool = prefab.GetPool();
                if (!_gameObjects.TryGetValue(instanceId, out var tuple))
                {
                    _copy[instanceId] = tuple = (pool, pool.Get());
                }
                else if(pool != tuple.Item1)
                {
                    if (tuple.Item2.TryGetComponent(out Presenter presenter))
                    {
                        presenter.StopPresent();
                    }
                    tuple.Item1.Release(tuple.Item2);
                    _gameObjects.Remove(instanceId);
                    _copy[instanceId] = tuple = (pool, pool.Get());
                }
                else
                {
                    _gameObjects.Remove(instanceId);
                    continue;
                }
                tuple.Item2.transform.SetParent(transform);
                tuple.Item2.transform.localScale = Vector3.one;
                if (tuple.Item2.TryGetComponent(out Presenter presenter2))
                {
                    presenter2.Present(child.gameObject);
                }
            }

            foreach (var keyValuePair in _gameObjects)
            {
                if (keyValuePair.Value.Item2.TryGetComponent(out Presenter presenter))
                {
                    presenter.StopPresent();
                }
                keyValuePair.Value.Item1.Release(keyValuePair.Value.Item2);
            }
            _gameObjects.Clear();
            (_gameObjects, _copy) = (_copy, _gameObjects);
        }
        
    }
    
    public interface IAssetProviderFor<out TOut, in TIn>
    {
        TOut Provide(TIn tIn);
    }
}