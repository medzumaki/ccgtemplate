﻿using System;
using System.Collections.Generic;
using Pixeye.Actors;
using Project.Code.Utility;
using UnityEngine;
using Utilities.Unity.Extensions;

namespace Project.Code.PresenterLayer
{
    public static class PresenterExtensions
    {
        public static void AddTo(this IDisposable disposable, Presenter presenter)
        {
            presenter.AddTo(disposable);
        }
    }
    
    [DisallowMultipleComponent]
    public class Presenter : MonoBehaviour
    {
        protected GameObject _presentableGameObject { get; private set; }

        [SerializeField] private List<Presenter> _childs = new List<Presenter>();
        private readonly List<IDisposable> _disposables = new List<IDisposable>();
        private bool _isPresented = false;


        protected virtual void Awake()
        {
            gameObject.SetActive(_isPresented);
        }

        public bool Present(GameObject presentableGameObject)
        {
            if (_isPresented = CheckAvailablePresent(presentableGameObject))
            {
                PresentHandler(presentableGameObject);
            }

            return _isPresented;
        }

        protected virtual bool CheckAvailablePresent(GameObject presentableGameObject)
        {
            return presentableGameObject;
        }

        protected virtual void PresentHandler(GameObject presentableGameObject)
        {
            _presentableGameObject = presentableGameObject;
            gameObject.SetActive(true);
            _childs.ForEach(x => x.Present(presentableGameObject));
        }

        public void StopPresent()
        {
            if (_isPresented)
            {
                StopPresentHandler();
                gameObject.SetActive(false);
            }
        }

        protected virtual void StopPresentHandler()
        {
            _childs.ForEach(x => x.StopPresent());
            _disposables.ForEach(x => x.Dispose());
            _disposables.Clear();
            _isPresented = false;
        }

        public void AddTo(IDisposable disposable)
        {
            _disposables.Add(disposable);
        }

        [Sirenix.OdinInspector.Button]
        protected virtual void Rebuild()
        {
            _childs.Clear();
            gameObject.CollectAllChild(_childs);
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }

        [Sirenix.OdinInspector.Button]
        protected virtual void OnValidate()
        {
            
        }
    }

    public class Presenter<T> : Presenter
    {
        protected T _model { get; private set; }

        protected override bool CheckAvailablePresent(GameObject presentableGameObject)
        {
            return (_model = presentableGameObject.GetComponent<T>()).IsNotNullInUnity();
        }
    }

    public class ActorPresenter<T> : Presenter<Actor>
    {
        protected T _actorComponent { get; private set; }

        protected sealed override bool CheckAvailablePresent(GameObject presentableGameObject)
        {
            if (base.CheckAvailablePresent(presentableGameObject) && _model.entity.Has<T>())
            {
                _actorComponent = _model.entity.Get<T>();
                return true;
            }
            return false;
        }
    }
}