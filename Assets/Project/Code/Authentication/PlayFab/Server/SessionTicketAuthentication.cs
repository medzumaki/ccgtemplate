﻿using FishNet.Broadcast;

namespace Project.Code.Authentication.PlayFab.Server
{
    public struct SessionTicketAuthentication : IBroadcast
    {
        public string SessionTicket;
    }
}