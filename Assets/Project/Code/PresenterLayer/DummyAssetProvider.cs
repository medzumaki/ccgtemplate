﻿using UnityEngine;

namespace Project.Code.PresenterLayer
{
    public class DummyAssetProvider : MonoBehaviour, IAssetProviderFor<GameObject, GameObject>
    {
        [SerializeField] private Presenter _presenter;
        public GameObject Provide(GameObject tIn)
        {
            return _presenter.gameObject;
        }
    }
}