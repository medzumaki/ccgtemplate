﻿using System;
using System.Linq;
using Pixeye.Actors;
using UniRx;
using UnityEngine;
using Utilities.Unity.PropertyAttributes;

namespace Project.Code.PresenterLayer
{
    public class PresenterInitializer<T> : Presenter where T : GroupProcessor, new()
    {
        private T _groupProcessor;

        protected override void Awake()
        {
            _groupProcessor = new T();
            _groupProcessor.Group()
                .ObserveEveryValueChanged(x =>
                {
                    foreach (var ent in x.added)
                    {
                        Debug.Log($"Added {ent.id}");
                    }
                    
                    foreach (var ent in x)
                    {
                        if (ent.transform)
                        {
                            return ent;
                        }
                    }

                    return default;
                })
                .Subscribe(x =>
                {
                    if (x.transform)
                    {
                        Present(x.transform.gameObject);
                    }
                    else
                    {
                        StopPresent();
                    }
                }).AddTo(gameObject);
        }
    }

    public abstract class GroupProcessor : Processor
    {
        public abstract GroupCore Group();
    }
    
    public sealed class ObservableProcessor<T> : GroupProcessor
    {
        private Group<T> _group;

        public override GroupCore Group() => _group;
        
        public ObservableProcessor() : base()
        {
            
        }
    }

    public sealed class ObservableProcessor<T1, T2> : GroupProcessor
    {
        private Group<T1, T2> _group;

        public override GroupCore Group() => _group;

        public ObservableProcessor() : base()
        {
            
        }
    }
}