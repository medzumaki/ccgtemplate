﻿using UniRx;

namespace Project.Code.Gameplay
{
    public interface IRareComponent
    {
        public IReadOnlyReactiveProperty<string> Rare { get; }
    }
}