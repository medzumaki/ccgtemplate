﻿using Project.Code.FishNet;

namespace Project.Code.Gameplay
{
    public class CreatureComponent : NetworkActor<ICreatureComponent>, ICreatureComponent
    {
        protected override ICreatureComponent ProvideComponent()
        {
            return this;
        }
    }
}