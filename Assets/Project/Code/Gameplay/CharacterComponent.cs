﻿using System.Runtime.CompilerServices;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using Pixeye.Actors;
using Project.Code.FishNet;
using Unity.IL2CPP.CompilerServices;

namespace Project.Code.Gameplay
{
    public class CharacterComponent : NetworkActor<CharacterComponent>
    {
        [field: SyncObject(ReadPermissions = ReadPermission.Observers, SendRate = 1)]
        public readonly SyncReactiveProperty<string> Name= new SyncReactiveProperty<string>();

        [field: SyncObject(ReadPermissions = ReadPermission.Observers, SendRate = 1)]
        public readonly SyncReactiveProperty<string> Id = new SyncReactiveProperty<string>();

        protected override CharacterComponent ProvideComponent()
        {
            return this;
        }
    }
    
    #region HELPERS

    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    static class CharacterHelper
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static ref CharacterComponent ComponentObserver(in this ent entity) =>
            ref Storage<CharacterComponent>.components[entity.id];
    }

    public class CharacterStorage : Storage<CharacterComponent>
    {
        public override void Dispose(indexes disposed)
        {
            
        }
    }
    #endregion
}