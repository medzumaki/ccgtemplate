﻿using System;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ServerModels;

namespace Project.Code.Gameplay.Character
{
    public interface ICharactersHandler
    {
        void Handle(CharactersComponent charactersComponent);
    }
}