﻿using Project.Code.FishNet;

namespace Project.Code.Gameplay
{
    public class SpellComponent : NetworkActor<ISpellComponent>, ISpellComponent
    {
        protected override ISpellComponent ProvideComponent()
        {
            return this;
        }
    }
}