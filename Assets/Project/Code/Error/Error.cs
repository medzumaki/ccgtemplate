﻿using FishNet.Broadcast;

namespace Project.Code.Error
{
    public struct Error : IBroadcast
    {
        public string ErrorKey;
        public string[] Arguments;
    }
}