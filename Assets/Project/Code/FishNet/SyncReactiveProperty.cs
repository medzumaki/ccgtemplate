﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using FishNet.Documenting;
using FishNet.Object.Synchronizing;
using FishNet.Object.Synchronizing.Internal;
using FishNet.Serializing;
using UniRx;
using UnityEngine;

namespace Project.Code.FishNet
{
    [Serializable]
    public class SyncReactiveProperty<T> : SyncBase, IReactiveProperty<T>
    {
        [SerializeField] private ReactiveProperty<T> _reactiveProperty = new ReactiveProperty<T>();
        private IEqualityComparer<T> _equalityComparer = EqualityComparer<T>.Default;

        private T _initialValue;
        private bool _valueChanged = false;
        private bool _sendAll;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [APIExclude]
        public override void Read(PooledReader reader)
        {
            /* When !asServer don't make changes if server is running.
            * This is because changes would have already been made on
            * the server side and doing so again would result in duplicates
            * and potentially overwrite data not yet sent. */
            bool asClientAndHost = base.NetworkManager.IsServer;
            if (base.NetworkManager.IsClient)
            {
                _reactiveProperty.Value = reader.Read<T>();
            }
        }

        public override void Reset()
        {
            base.Reset();
            _reactiveProperty.Value = _initialValue;
        }

        public override void WriteDelta(PooledWriter writer, bool resetSyncTick = true)
        {
            base.WriteDelta(writer, resetSyncTick);
            writer.Write(_reactiveProperty.Value);
        }

        public override void WriteFull(PooledWriter writer)
        {
            base.WriteHeader(writer, false);
            writer.Write(_reactiveProperty.Value);
        }

        protected override void Registered()
        {
            base.Registered();
            _initialValue = _reactiveProperty.Value;
        }

        protected override void WriteHeader(PooledWriter writer, bool resetSyncTick = true)
        {
            base.WriteHeader(writer, resetSyncTick);
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            return _reactiveProperty.Subscribe(observer);
        }

        public T Value
        {
            get => _reactiveProperty.Value;
            set
            {
                _reactiveProperty.Value = value;
                if(!base.IsRegistered)
                    return;
                if (base.NetworkManager != null && base.Settings.WritePermission == WritePermission.ServerOnly
                                                && !base.NetworkBehaviour.IsServer)
                {
                    base.NetworkManager.LogWarning($"Cannot complete operation as server when server is not active.");
                    return;
                }

                _valueChanged = true;
                base.Dirty();
            }
        }

        public bool HasValue => _reactiveProperty.HasValue;
    }
}