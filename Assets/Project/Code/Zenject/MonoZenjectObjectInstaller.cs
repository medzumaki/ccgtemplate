﻿using UnityEngine;
using Zenject;

namespace Project.Code.Zenject
{
    public class MonoZenjectObjectInstaller : MonoInstaller
    {
        [SerializeField] private Object[] _objects;

        public override void InstallBindings()
        {
            foreach (var variable in _objects)
            {
                Container.BindInterfacesAndSelfTo(variable.GetType())
                    .FromInstance(variable)
                    .AsCached();
            }
        }
    }
}