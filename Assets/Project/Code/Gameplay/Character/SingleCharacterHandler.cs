﻿using FishNet.Utility;
using PlayFab.ServerModels;
using UnityEngine;

namespace Project.Code.Gameplay.Character
{
    [RequireComponent(typeof(PlayFabComponent))]
    public class SingleCharacterHandler : MonoBehaviour, ICharactersHandler
    {
        private PlayFabComponent _playFabComponent;
        [Scene] 
        private string _characterScene;

        private void Awake()
        {
            _playFabComponent = GetComponent<PlayFabComponent>();
        }

        public void Handle(CharactersComponent charactersComponent)
        {
#if ENABLE_PLAYFABSERVER_API
            if (charactersComponent.CharactersList.Count == 0)
            {
                charactersComponent.CreateCharacter(_playFabComponent.PlayFabId.Value, "Default");
            }
#endif
        }
    }
}