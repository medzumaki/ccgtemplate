﻿using System;
using System.Runtime.CompilerServices;
using FishNet.Object;
using Pixeye.Actors;
using Project.Code.FishNet;
using UniRx;
using Unity.IL2CPP.CompilerServices;
using UnityEngine.Scripting;

namespace Project.Code.Gameplay
{
    public class ClientComponent : NetworkActor<ClientComponent>
    {
        protected override ClientComponent ProvideComponent()
        {
            return this;
        }
    }
    
    #region HELPERS

    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Preserve]
    static class ClientHelper
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [Preserve]
        internal static ref ClientComponent ComponentObserver(in this ent entity) =>
            ref Storage<ClientComponent>.components[entity.id];
    }
    
    public class ClientStorage : Storage<ClientComponent>
    {
        public override void Dispose(indexes disposed)
        {
            
        }
    }
    #endregion
}