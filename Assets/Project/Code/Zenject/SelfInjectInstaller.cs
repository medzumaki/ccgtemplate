﻿using UnityEngine;
using Zenject;

namespace Project.Code.Zenject
{
    [CreateAssetMenu]
    public class SelfInjectInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind(typeof(DiContainer), typeof(IInstantiator))
                .FromInstance(Container)
                .AsSingle();
        }
    }
}