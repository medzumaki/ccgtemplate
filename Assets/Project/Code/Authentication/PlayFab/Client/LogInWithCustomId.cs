﻿using FishNet.Managing;
using PlayFab;
using PlayFab.ClientModels;
using Project.Code.Authentication.PlayFab.Server;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Project.Code.Authentication.PlayFab.Client
{
    public class LogInWithCustomId : MonoBehaviour
    {
        [SerializeField] private Button _button;
        [SerializeField] private TMP_InputField _customIdField;

        [Inject]
        private void Inject(NetworkManager networkManager)
        {
            _button.OnClickAsObservable()
                .Subscribe(async x =>
                {
                    _button.interactable = false;
                    PlayFabClientAPI.LoginWithCustomID(new LoginWithCustomIDRequest()
                    {
                        CreateAccount = false,
                        CustomId = _customIdField.text
                    }, x =>
                    {
                        Debug.Log(x.SessionTicket);
                        Debug.Log(x.PlayFabId);
                        networkManager.ClientManager.Broadcast(new SessionTicketAuthentication()
                        {
                            SessionTicket = x.SessionTicket
                        });
                        Debug.Log("Success");
                    }, y =>
                    {
                        Debug.LogError(y.ErrorMessage);
                        _button.interactable = true;
                    });
                })
                .AddTo(this);
        }
    }
}