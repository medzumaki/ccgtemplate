﻿using Project.Code.FishNet;
using UniRx;
using UnityEngine;

namespace Project.Code.Gameplay
{
    public class RareComponent : NetworkActor<IRareComponent>, IRareComponent
    {
        [SerializeField]
        private ReactiveProperty<string> _rare;

        protected override IRareComponent ProvideComponent()
        {
            return this;
        }

        public IReadOnlyReactiveProperty<string> Rare => _rare.ToReadOnlyReactiveProperty();
    }
}