﻿using System;
using System.Collections.Generic;
using System.Linq;
using Project.Code.Utility;
using Sirenix.OdinInspector;
using UnityEngine;
using Utilities.Unity.Buttons;
using Utilities.Unity.Extensions;
using Utilities.Unity.PropertyAttributes;

namespace Project.Code.PresenterLayer
{
    public class SelectWidgetPresenter : Presenter
    {
        [SerializeField] private List<Presenter> _presenters;
        [ReadOnlyField]
        [SerializeField]
        private NonPresenter _nonPresenter;

        [SerializeField] private Presenter _selectedPresenter = null;

        private Presenter _initialValue = null;

        protected override void OnValidate()
        {
            if (!_nonPresenter)
            {
                var gameObject = new GameObject("SelectWidgetPresenterRoot");
                if (transform is RectTransform rectTransform)
                {
                    rectTransform = _nonPresenter.GetOrAddComponent<RectTransform>();
                    rectTransform.anchorMax = Vector2.one;
                    rectTransform.anchorMin = Vector2.zero;
                    rectTransform.anchoredPosition = Vector2.zero;
                }
                else
                {
                    gameObject.transform.SetParent(transform);
                }
                _nonPresenter = gameObject.AddComponent<NonPresenter>();
                #if UNITY_EDITOR
                UnityEditor.EditorUtility.SetDirty(this);
                #endif
            }
            if (!_presenters.Contains(_selectedPresenter))
            {
                _selectedPresenter = null;
            }
        }

        [Sirenix.OdinInspector.Button]
        private void CollectSelectablePresenters()
        {
            _presenters.Clear();
            _nonPresenter.CollectAllChildForEditor(_presenters);
        }

        protected override void Awake()
        {
            base.Awake();
            _initialValue = _selectedPresenter;
        }

        protected override void PresentHandler(GameObject presentableGameObject)
        {
             base.PresentHandler(presentableGameObject);
            _selectedPresenter = _initialValue;
            _presenters.ForEach(x =>
            {
                if (x != _selectedPresenter)
                {
                    x?.StopPresent();
                }
                else
                {
                    x?.Present(presentableGameObject);
                }
            });
        }

        public void Select(string presenter)
        {
            _selectedPresenter?.StopPresent();
            _selectedPresenter = _presenters.FirstOrDefault(x => x.name == presenter);
            _selectedPresenter?.Present(_presentableGameObject);
        }
    }
}