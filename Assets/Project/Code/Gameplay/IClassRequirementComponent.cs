﻿using UniRx;

namespace Project.Code.Gameplay
{
    public interface IClassRequirementComponent
    {
        public ReadOnlyReactiveProperty<string> ClassName { get; }
    }
}