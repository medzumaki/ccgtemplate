﻿using System;
using System.Collections.Generic;
using System.Linq;
using FishNet;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using PlayFab;
using PlayFab.ServerModels;
using Project.Code.FishNet;
using UnityEngine;

namespace Project.Code.Gameplay
{
    public class ItemsComponent : NetworkActor<ItemsComponent>
    {
        [SerializeField] private List<NetworkObject> NetworkObjects;
        [field: SyncObject] public SyncDictionary<string, NetworkObject> Items { get; } = new SyncDictionary<string, NetworkObject>();

        public void OnServerInitialized()
        {
            #if ENABLE_PLAYFABSERVER_API
            PlayFabServerAPI.GetCatalogItems(new GetCatalogItemsRequest(), x =>
            {
                foreach (var catalogItem in x.Catalog)
                {
                    if (NetworkObjects.FirstOrDefault(x => x.name == catalogItem.ItemId) is { } nob)
                    {
                        nob = NetworkManager.GetPooledInstantiated(nob, true);
                        ServerManager.Spawn(nob);
                        Items[catalogItem.ItemId] = nob;   
                    }
                }
            }, y =>
            {
                
            });
            #endif
        }

        protected override ItemsComponent ProvideComponent()
        {
            return this;
        }
    }
}