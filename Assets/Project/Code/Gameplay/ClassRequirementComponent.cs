﻿using Project.Code.FishNet;
using UniRx;
using UnityEngine;

namespace Project.Code.Gameplay
{
    public class ClassRequirementComponent : NetworkActor<IClassRequirementComponent>, IClassRequirementComponent
    {
        [SerializeField] 
        private ReactiveProperty<string> _className = new ReactiveProperty<string>();

        protected override IClassRequirementComponent ProvideComponent()
        {
            return this;
        }
        
        public ReadOnlyReactiveProperty<string> ClassName => _className.ToReadOnlyReactiveProperty();
    }
}