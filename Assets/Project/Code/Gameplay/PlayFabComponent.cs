﻿using System.Runtime.CompilerServices;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using Pixeye.Actors;
using PlayFab;
using PlayFab.ServerModels;
using Project.Code.Authentication.PlayFab.Server;
using Project.Code.FishNet;
using Unity.IL2CPP.CompilerServices;
using UnityEngine.Scripting;
using Zenject;

namespace Project.Code.Gameplay
{
    public class PlayFabComponent : NetworkActor<PlayFabComponent>
    {
        private PlayFabAuthenticator _playFabAuthenticator;

        [field: SyncObject(ReadPermissions = ReadPermission.OwnerOnly, SendRate = 1)]
        public readonly SyncReactiveProperty<string> PlayFabId = new SyncReactiveProperty<string>();

        [field: SyncObject(ReadPermissions = ReadPermission.Observers, SendRate = 1)]
        public readonly SyncReactiveProperty<string> PlayFabName = new SyncReactiveProperty<string>();

        [Inject]
        private void Inject(PlayFabAuthenticator playFabAuthenticator)
        {
            _playFabAuthenticator = playFabAuthenticator;
        }

        public override void OnSpawnServer(NetworkConnection connection)
        {
            base.OnSpawnServer(connection);
#if ENABLE_PLAYFABSERVER_API
            if (_playFabAuthenticator.NetworkConnections.TryGetValue(connection, out var key))
            {
                PlayFabId.Value = key;
                PlayFabServerAPI.GetUserAccountInfo(new GetUserAccountInfoRequest()
                {
                    PlayFabId = PlayFabId.Value
                }, x =>
                {
                    PlayFabName.Value = x.UserInfo.Username;
                }, y =>
                {
                    
                });
            }
#endif
        }

        protected override PlayFabComponent ProvideComponent()
        {
            return this;
        }
    }
    
    #region HELPERS

    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Preserve]
    static class PlayFabHelper
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [Preserve]
        internal static ref PlayFabComponent ComponentObserver(in this ent entity) =>
            ref Storage<PlayFabComponent>.components[entity.id];
    }
    
    public class PlayFabStorage : Storage<PlayFabComponent>
    {
        public override void Dispose(indexes disposed)
        {
            
        }
    }
    #endregion
}