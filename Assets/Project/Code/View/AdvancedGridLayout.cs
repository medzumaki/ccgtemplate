﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using TMPro;

namespace Scripts.UI.TripletUIGrid
{
    public enum Alignment
    {
        UpperLeft = 0,
        UpperCenter,
        UpperRight,
        MiddleLeft,
        MiddleCenter,
        MiddleRight,
        LowerLeft,
        LowerCenter,
        LowerRight
    }

    public enum Constraint
    {
        Flexible = 0,
        FixedColumnCount,
        FixedRowCount
    }

    public enum Axis
    {
        Horizontal,
        Vertical
    }
    
    [ExecuteAlways]
    public class AdvancedGridLayout : LayoutGroup
    {
        [SerializeField][FormerlySerializedAs("_cellSize")]
        private Vector2 _cellSize = new Vector2(100, 100);
        [SerializeField] private Vector2 _spacing = new Vector2(5, 5);

        [SerializeField] private Constraint _constraint;
        [SerializeField] private Axis _axis;
        [SerializeField][Min(1)] private int _constraintCount = 1;

        [SerializeField]
        private bool _isExpanded = false;

        private struct CalculateData
        {
            public int Min;
            public int Prefered;

            public CalculateData(int min, int prefered)
            {
                Min = min;
                Prefered = prefered;
            }
        }
        
        private struct AlignmentCalculateData
        {
            public Vector2 StartPosition;
            public Vector2 Direction;
        }
        
        private struct TableData
        {
            public int Columns;
            public int Rows;
        }

        private static Action<AdvancedGridLayout, TableData>[] _alignmentCalculations =
            new Action<AdvancedGridLayout, TableData>[]
            {
                UpperLeftCalculation,
                UpperCenterCalculation,
                UpperRightCalculation,
                MiddleLeftCalculation,
                MiddleCenterCalculation,
                MiddleRightCalculation,
                LowerLeftCalculation,
                LowerCenterCalculation,
                LowerRightCalculation
            };

        private static Vector2 CalculateSize(AdvancedGridLayout advancedGridLayout, TableData tableData)
        {
            var cellSize = advancedGridLayout._cellSize;
            if (advancedGridLayout._isExpanded)
            {
                var rectSize = advancedGridLayout.rectTransform.rect.size;
                var aspect = cellSize.x / cellSize.y;

                var spacingY = (tableData.Rows - 1) * advancedGridLayout._spacing.y + advancedGridLayout.padding.vertical;
                var rowSize = (rectSize.y - spacingY) / tableData.Rows;
                var spacingX = (tableData.Columns - 1) * advancedGridLayout._spacing.x +
                               advancedGridLayout.padding.horizontal;
                var columnSize = (rectSize.x - spacingX) / tableData.Columns;
                
                var availableSpaceX = rectSize.x - advancedGridLayout.padding.horizontal - tableData.Columns * rowSize * aspect -
                                      (tableData.Columns - 1) * advancedGridLayout._spacing.x;
                var availableSpaceY = rectSize.y - advancedGridLayout.padding.vertical - tableData.Rows * columnSize / aspect
                                      - (tableData.Rows - 1) * advancedGridLayout._spacing.y;
                
                if (availableSpaceX > availableSpaceY)
                {

                    return new Vector2(aspect * rowSize, rowSize);
                }
                else
                {
                    return new Vector2(columnSize, columnSize / aspect);
                }
            }

            return cellSize;
        }
        
        private static void MiddleRightCalculation(AdvancedGridLayout advancedGridLayout, TableData tableData)
        {
            var childs = advancedGridLayout.rectChildren;
            var spacing = advancedGridLayout._spacing;
            var cellSize = CalculateSize(advancedGridLayout, tableData);
            var startPos = new Vector2(advancedGridLayout.rectTransform.rect.width - advancedGridLayout.padding.right - cellSize.x, advancedGridLayout.rectTransform.rect.height/2f - cellSize.y/2f);
            var axis = advancedGridLayout._axis;
            if (axis == Axis.Horizontal)
            {
                int maxRows = (childs.Count - 1) / tableData.Columns + 1;
                for (int i = 0; i < childs.Count; i++)
                {
                    int positionX = i % tableData.Columns;
                    float positionY = i / tableData.Columns;
                    positionY = positionY - (maxRows - 1)/2f;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x - (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
            else
            {
                int maxColumns = childs.Count / tableData.Rows;
                for (int i = 0; i < childs.Count; i++)
                {
                    
                    float positionY = i % tableData.Rows;
                    int positionX = i / tableData.Rows;
                    if (positionX < maxColumns)
                    {
                        positionY = positionY - (tableData.Rows - 1) / 2f;
                    }
                    else
                    {
                        int availableCells = childs.Count - positionX * tableData.Rows;
                        positionY = positionY - (availableCells - 1) / 2f;
                    }
                    
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x - (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
        }

        private static void LowerRightCalculation(AdvancedGridLayout advancedGridLayout, TableData tableData)
        {
            var childs = advancedGridLayout.rectChildren;
            var spacing = advancedGridLayout._spacing;
            var cellSize = CalculateSize(advancedGridLayout, tableData);
            var startPos = new Vector2(advancedGridLayout.rectTransform.rect.width - advancedGridLayout.padding.right - cellSize.x, advancedGridLayout.rectTransform.rect.height - advancedGridLayout.padding.bottom - cellSize.y);
            var axis = advancedGridLayout._axis;
            if (axis == Axis.Horizontal)
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    int positionX = i % tableData.Columns;
                    int positionY = i / tableData.Columns;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x - (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y - (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
            else
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    int positionX = i / tableData.Rows;
                    int positionY = i % tableData.Rows;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x - (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y - (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
        }

        private static void LowerCenterCalculation(AdvancedGridLayout advancedGridLayout, TableData tableData)
        {
            var childs = advancedGridLayout.rectChildren;
            var spacing = advancedGridLayout._spacing;
            var cellSize = CalculateSize(advancedGridLayout, tableData);
            var rect = advancedGridLayout.rectTransform.rect;
            var startPos = new Vector2(rect.width/2f - cellSize.x/2f, rect.height - advancedGridLayout.padding.bottom - cellSize.y);
            var axis = advancedGridLayout._axis;
            if (axis == Axis.Horizontal)
            {
                int maxRows = childs.Count / tableData.Columns;
                for (int i = 0; i < childs.Count; i++)
                {
                    
                    int positionY = i / tableData.Columns;
                    float positionX = i % tableData.Columns;
                    if (positionY < maxRows)
                    {
                        positionX = positionX - (tableData.Columns - 1) / 2f;
                    }
                    else
                    {
                        int availableCells = childs.Count - positionY * tableData.Columns;
                        positionX = positionX - (availableCells - 1) / 2f;
                    }
                    
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y - (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
            else
            {
                int maxColumns = (childs.Count - 1) / tableData.Rows + 1;
                for (int i = 0; i < childs.Count; i++)
                {
                    float positionX = i / tableData.Rows;
                    positionX = positionX - (maxColumns - 1)/2f;
                    int positionY = i % tableData.Rows;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y - (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
        }

        private static void LowerLeftCalculation(AdvancedGridLayout advancedGridLayout, TableData tableData)
        {
            var childs = advancedGridLayout.rectChildren;
            var spacing = advancedGridLayout._spacing;
            var cellSize = CalculateSize(advancedGridLayout, tableData);
            var startPos = new Vector2(advancedGridLayout.padding.left, advancedGridLayout.rectTransform.rect.height - advancedGridLayout.padding.bottom - cellSize.y);
            var axis = advancedGridLayout._axis;
            if (axis == Axis.Horizontal)
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    int positionX = i % tableData.Columns;
                    int positionY = i / tableData.Columns;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y - (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
            else
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    int positionX = i / tableData.Rows;
                    int positionY = i % tableData.Rows;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y - (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
        }

        private static void MiddleCenterCalculation(AdvancedGridLayout advancedGridLayout, TableData tableData)
        {
            var childs = advancedGridLayout.rectChildren;
            var spacing = advancedGridLayout._spacing;
            var cellSize = CalculateSize(advancedGridLayout, tableData);
            var rect = advancedGridLayout.rectTransform.rect;
            var startPos = new Vector2(rect.width/2f - cellSize.x/2f, rect.height/2f - cellSize.y/2f);
            var axis = advancedGridLayout._axis;
            if (axis == Axis.Horizontal)
            {
                int maxRowsForColumns = childs.Count / tableData.Columns;
                int maxRowsForRows = (childs.Count - 1) / tableData.Columns + 1;
                for (int i = 0; i < childs.Count; i++)
                {
                    
                    int row = i / tableData.Columns;
                    float positionX = i % tableData.Columns;
                    if (row < maxRowsForColumns)
                    {
                        positionX = positionX - (tableData.Columns - 1) / 2f;
                    }
                    else
                    {
                        int availableCells = childs.Count - row * tableData.Columns;
                        positionX = positionX - (availableCells - 1) / 2f;
                    }
                    
                    float positionY = row - (maxRowsForRows - 1)/2f;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
            else
            {
                int maxColumnsForRows = childs.Count / tableData.Rows;
                int maxColumnsForColumns = (childs.Count - 1) / tableData.Rows + 1;
                for (int i = 0; i < childs.Count; i++)
                {
                    
                    float positionY = i % tableData.Rows;
                    int column = i / tableData.Rows;
                    if (column < maxColumnsForRows)
                    {
                        positionY = positionY - (tableData.Rows - 1) / 2f;
                    }
                    else
                    {
                        int availableCells = childs.Count - column * tableData.Rows;
                        positionY = positionY - (availableCells - 1) / 2f;
                    }

                    float positionX = column - (maxColumnsForColumns - 1) / 2f;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
        }

        private static void MiddleLeftCalculation(AdvancedGridLayout advancedGridLayout, TableData tableData)
        {
            var childs = advancedGridLayout.rectChildren;
            var spacing = advancedGridLayout._spacing;
            var cellSize = CalculateSize(advancedGridLayout, tableData);
            var startPos = new Vector2(advancedGridLayout.padding.left, advancedGridLayout.rectTransform.rect.height/2f - cellSize.y/2f);
            var axis = advancedGridLayout._axis;
            if (axis == Axis.Horizontal)
            {
                int maxRows = (childs.Count - 1) / tableData.Columns + 1;
                for (int i = 0; i < childs.Count; i++)
                {
                    int positionX = i % tableData.Columns;
                    float positionY = i / tableData.Columns;
                    positionY = positionY - (maxRows - 1)/2f;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
            else
            {
                int maxColumns = childs.Count / tableData.Rows;
                for (int i = 0; i < childs.Count; i++)
                {
                    
                    int positionY = i % tableData.Rows;
                    int positionX = i / tableData.Rows;
                    if (positionX < maxColumns)
                    {
                        positionY = positionY - (tableData.Rows - 1) / 2;
                    }
                    else
                    {
                        int availableCells = childs.Count - positionX * tableData.Rows;
                        positionY = positionY - availableCells / 2;    
                    }
                    
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*(positionY - 0.5f), cellSize.y);
                }
            }
        }

        private static void UpperRightCalculation(AdvancedGridLayout advancedGridLayout, TableData tableData)
        {
            var childs = advancedGridLayout.rectChildren;
            var spacing = advancedGridLayout._spacing;
            var cellSize = CalculateSize(advancedGridLayout, tableData);
            var startPos = new Vector2(advancedGridLayout.rectTransform.rect.width - advancedGridLayout.padding.right - cellSize.x, advancedGridLayout.padding.top);
            var axis = advancedGridLayout._axis;
            if (axis == Axis.Horizontal)
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    int positionX = i % tableData.Columns;
                    int positionY = i / tableData.Columns;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x - (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
            else
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    int positionX = i / tableData.Rows;
                    int positionY = i % tableData.Rows;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x - (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
        }

        private static void UpperCenterCalculation(AdvancedGridLayout advancedGridLayout, TableData tableData)
        {
            var childs = advancedGridLayout.rectChildren;
            var spacing = advancedGridLayout._spacing;
            var cellSize = CalculateSize(advancedGridLayout, tableData);
            var startPos = new Vector2(advancedGridLayout.rectTransform.rect.width/2f - cellSize.x/2f, advancedGridLayout.padding.top);
            var axis = advancedGridLayout._axis;
            if (axis == Axis.Horizontal)
            {
                int maxRows = childs.Count / tableData.Columns;
                for (int i = 0; i < childs.Count; i++)
                {
                    
                    int positionY = i / tableData.Columns;
                    float positionX = i % tableData.Columns;
                    if (positionY < maxRows)
                    {
                        positionX = positionX - (tableData.Columns - 1) / 2f;
                    }
                    else
                    {
                        int availableCells = childs.Count - positionY * tableData.Columns;
                        positionX = positionX - (availableCells - 1) / 2f;
                    }
                    
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
            else
            {
                int maxColumns = (childs.Count - 1) / tableData.Rows + 1;
                for (int i = 0; i < childs.Count; i++)
                {
                    float positionX = i / tableData.Rows;
                    positionX = positionX - (maxColumns - 1)/2f;
                    int positionY = i % tableData.Rows;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
        }

        private static void UpperLeftCalculation(AdvancedGridLayout advancedGridLayout, TableData tableData)
        {
            var childs = advancedGridLayout.rectChildren;
            var startPos = new Vector2(advancedGridLayout.padding.left, advancedGridLayout.padding.top);
            var spacing = advancedGridLayout._spacing;
            var cellSize = CalculateSize(advancedGridLayout, tableData);
            var axis = advancedGridLayout._axis;
            if (axis == Axis.Horizontal)
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    int positionX = i % tableData.Columns;
                    int positionY = i / tableData.Columns;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
            else
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    int positionX = i / tableData.Rows;
                    int positionY = i % tableData.Rows;
                    
                    advancedGridLayout.SetChildAlongAxis(childs[i], 0, startPos.x + (cellSize.x + spacing.x)*positionX, cellSize.x);
                    advancedGridLayout.SetChildAlongAxis(childs[i], 1, startPos.y + (cellSize.y + spacing.y)*positionY, cellSize.y);
                }
            }
        }

        private static Func<AdvancedGridLayout, CalculateData>[] _horizontalCalculateActions = new Func<AdvancedGridLayout, CalculateData>[]
        {
            FlexibleColumnCountCalculate,
            FixedCountCalculate,
            FixedCountRelativeCalculate
        };
        
        private static Func<AdvancedGridLayout, CalculateData>[] _verticalCalculateActions = new Func<AdvancedGridLayout, CalculateData>[]
        {
            FlexibleRowCountCalculate,
            FixedCountRelativeCalculate,
            FixedCountCalculate
        };

        private static CalculateData FlexibleRowCountCalculate(AdvancedGridLayout arg)
        {
            var width = arg.rectTransform.rect.width;
            var padding = arg.padding;
            var cellSize = arg._cellSize;
            var spacing = arg._spacing;
            int cellCountX = Mathf.Max(1,
                Mathf.FloorToInt((width - padding.horizontal + spacing.x + 0.001f) / (cellSize.x + spacing.x)));
            return new CalculateData(cellCountX, cellCountX);
        }

        private static CalculateData FixedCountRelativeCalculate(AdvancedGridLayout arg)
        {
            var columns = Mathf.CeilToInt(arg.rectChildren.Count / (float) arg._constraintCount - 0.001f);
            return new CalculateData(columns, columns);
        }

        private static CalculateData FlexibleColumnCountCalculate(AdvancedGridLayout arg)
        {
            var columns = 1;
            var rows = Mathf.CeilToInt(Mathf.Sqrt(arg.rectChildren.Count));
            return new CalculateData(columns, rows);
        }

        private static CalculateData FixedCountCalculate(AdvancedGridLayout arg)
        {
            return new CalculateData(arg._constraintCount, arg._constraintCount);
        }

        public override void CalculateLayoutInputVertical()
        {
            var data = _verticalCalculateActions[(int) _constraint](this);
            var minSpace = padding.vertical + (_cellSize.y + _spacing.y) * data.Min - _spacing.y;
            SetLayoutInputForAxis(minSpace, minSpace, -1, 1);
        }

        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();
            var calculateData = _horizontalCalculateActions[(int) _constraint](this);
            SetLayoutInputForAxis(padding.horizontal + (_cellSize.x + _spacing.x) * calculateData.Min - _spacing.x,
                padding.horizontal + (_cellSize.x + _spacing.x)*calculateData.Prefered - _spacing.x,
                -1, 0);
        }

        public override void SetLayoutHorizontal()
        {
            SetCellAlongAxis(0);
        }

        public override void SetLayoutVertical()
        {
            SetCellAlongAxis(1);
        }

        private void SetCellAlongAxis(int axis)
        {
            var rectChildrenCount = rectChildren.Count;
            if (axis == 0)
            {
                for (int i = 0; i < rectChildrenCount; i++)
                {
                    var childRectTransform = rectChildren[i];
                    
                    m_Tracker.Add(this, childRectTransform, DrivenTransformProperties.Anchors 
                                                            | DrivenTransformProperties.AnchoredPosition
                                                            | DrivenTransformProperties.SizeDelta);
                    childRectTransform.anchorMin = Vector2.up;
                    childRectTransform.anchorMax = Vector2.up;
                    childRectTransform.sizeDelta = _cellSize;
                }
                return;
            }

            var rect = rectTransform.rect;
            float width = rect.size.x;
            float height = rect.size.y;

            int cellCountX = 1;
            int cellCountY = 1;
            if (_constraint == Constraint.FixedColumnCount)
            {
                cellCountX = _constraintCount;

                if (rectChildrenCount > cellCountX)
                    cellCountY = rectChildrenCount / cellCountX + (rectChildrenCount % cellCountX > 0 ? 1 : 0);
            }
            else if (_constraint == Constraint.FixedRowCount)
            {
                cellCountY = _constraintCount;

                if (rectChildrenCount > cellCountY)
                    cellCountX = rectChildrenCount / cellCountY + (rectChildrenCount % cellCountY > 0 ? 1 : 0);
            }
            else
            {
                if (_cellSize.x + _spacing.x <= 0)
                    cellCountX = int.MaxValue;
                else
                    cellCountX = Mathf.Max(1, Math.Min(transform.childCount, Mathf.FloorToInt((width - padding.horizontal + _spacing.x + 0.001f) / (_cellSize.x + _spacing.x))));

                if (_cellSize.y + _spacing.y <= 0)
                    cellCountY = int.MaxValue;
                else
                    cellCountY = Mathf.Max(1, Mathf.Min(transform.childCount, Mathf.FloorToInt((height - padding.vertical + _spacing.y + 0.001f) / (_cellSize.y + _spacing.y))));
            }

            _alignmentCalculations[(int)m_ChildAlignment](this, new TableData() {Columns = cellCountX, Rows = cellCountY});
        }
    }
}