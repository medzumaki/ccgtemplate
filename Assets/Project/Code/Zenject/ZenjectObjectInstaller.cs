﻿using UnityEngine;
using Zenject;

namespace Project.Code.Zenject
{
    [CreateAssetMenu]
    public class ZenjectObjectInstaller : ScriptableObjectInstaller
    {
        [SerializeField] private Object[] _objects;

        public override void InstallBindings()
        {
            foreach (var variable in _objects)
            {
                Container.BindInterfacesAndSelfTo(variable.GetType())
                    .FromInstance(variable)
                    .AsCached();
            }
        }
    }
}