﻿using UnityEngine;

namespace Project.Code.FishNet
{
    [CreateAssetMenu(fileName = "ClientCondition")]
    public class ClientCondition : NetworkActor.Condition
    {
        public override bool IsEnabled(NetworkActor networkActor)
        {
            return networkActor.IsClient;
        }
    }
}